import {Component, Input, OnInit} from '@angular/core';
import {ListService} from '../list.service';
import {TodoItem} from '../interfaces/todo-item';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent implements OnInit {

  @Input() item: TodoItem;

  editing = false;


  constructor(
    public listService: ListService
  ) {

  }

  ngOnInit() {
  }

  removeItem() {
    this.listService.removeList(this.item);
  }

  changeComlited($event) {
    this.item.completed = $event.checked;

    console.log('2 this.item.completed', this.item.completed);
    this.listService.updateItem(this.item);
  }

  updateItem($event) {
    this.item.title = $event;
    this.listService.updateItem(this.item);
    this.editing = false;
  }

  setEditing($event) {
    console.log($event);

    $event.stopPropagation();
    $event.preventDefault();

    this.editing = true;
  }

}
