import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ListItemComponent } from './list-item/list-item.component';
import { ToDoListComponent } from './to-do-list/to-do-list.component';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { FlexLayoutModule } from '@angular/flex-layout';
import {MatButtonModule} from '@angular/material/button';
import { AddItemComponent } from './add-item/add-item.component';
import {MatInputModule} from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { InputButtonUnitComponent } from './input-button-unit/input-button-unit.component';
import {MatCardModule} from '@angular/material';

@NgModule({
  declarations: [
    AppComponent,
    ListItemComponent,
    ToDoListComponent,
    AddItemComponent,
    InputButtonUnitComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatCheckboxModule,
    FlexLayoutModule,
    MatButtonModule,
    MatInputModule,
    FormsModule,
    MatCardModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
