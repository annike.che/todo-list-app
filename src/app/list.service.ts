import { Injectable } from '@angular/core';
import {StorageItemService} from './storage-item.service';
import {TodoItem} from './interfaces/todo-item';

@Injectable({
  providedIn: 'root'
})
export class ListService {

  public defaultTodoList: Array<TodoItem> = [
    { title: 'firstTodo'},
    { title: 'two ToDo Item'}];
  public listToDo: Array<any>;

  private todoListStorageKey = 'MyToDoList';

  constructor(
    private storageService: StorageItemService
  ) {
    this.listToDo =
      storageService.getData(this.todoListStorageKey) || this.defaultTodoList;
  }

  public getList(): Array<any> {
    return this.listToDo;
  }

  public removeList(item): void {
    const index = this.listToDo.indexOf(item);
    this.listToDo.splice(index, 1);
    this.saveStorage();
  }

  public addItem(title): void {
    this.listToDo.push({title});
    this.saveStorage();
  }

  public updateItem(item): void {
    const index = this.listToDo.indexOf(item);
    this.listToDo[index] = item;
    this.saveStorage();

  }

  private saveStorage() {
    this.storageService.setData(this.todoListStorageKey, this.listToDo);
  }
}
