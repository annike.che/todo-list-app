import { TestBed } from '@angular/core/testing';

import { StorageItemService } from './storage-item.service';

describe('StorageItemService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StorageItemService = TestBed.get(StorageItemService);
    expect(service).toBeTruthy();
  });
});
