import { Component, OnInit } from '@angular/core';
import {ListService} from '../list.service';
import {TodoItem} from '../interfaces/todo-item';


@Component({
  selector: 'app-to-do-list',
  templateUrl: './to-do-list.component.html',
  styleUrls: ['./to-do-list.component.scss']
})
export class ToDoListComponent implements OnInit {

  public myList: Array<TodoItem>;

  constructor(
    public listService: ListService) { }

  ngOnInit() {
    this.getListTodo();

  }
  getListTodo(): void  {
      this.myList = this.listService.getList();
  }

}
