import { Component, OnInit } from '@angular/core';
import {ListService} from '../list.service';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.scss']
})
export class AddItemComponent implements OnInit {

  public title: string;

  constructor(
    public listService: ListService
  ) { }

  ngOnInit() {
  }

  addItem($event): any {
    console.log('$event', $event);

    if(!$event) return;
    this.title = $event;
    this.listService.addItem(this.title);
    this.title = '';
  }

}
